﻿using UnityEngine.UI;
using UnityEngine;

public class WarningUI : MonoBehaviour
{
    #region singleton creation
    private static WarningUI singleton = null;
    private void Start()
    {
        GetSingleton();
    }
    public static WarningUI GetSingleton()
    {
        if (singleton == null)
        {
            singleton = GameObject.Find("WarningUI").GetComponent<WarningUI>();
        }
        return singleton;
    }
    #endregion

    [SerializeField] private Text warningText;

    private float timeToDeactivate;

    public void Enable(string t)
    {
        warningText.text = t;
        timeToDeactivate = 5f;
        gameObject.SetActive(true);
    }

    private void Update()
    {
        timeToDeactivate -= Time.deltaTime;
        if(timeToDeactivate <= 0)
        {
            gameObject.SetActive(false);
        }
    }
}
