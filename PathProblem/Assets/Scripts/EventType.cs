﻿public enum EventType
{
    None,
    Block,
    Spawner,
    ReachPoint
}
