﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameUtility
{
    public class GameController : MonoBehaviour
    {
        #region singleton creation
        private static GameController singleton = null;
        public static GameController GetSingleton()
        {
            if (singleton == null)
            {
                singleton = GameObject.Find("GameController").GetComponent<GameController>();
            }
            return singleton;
        }
        #endregion

        #region variables
        public EventType CurrentMode; //Current event mode (adding spawner, block etc. or none)
        public bool IsGameStarted; //Checker if the game started
        #endregion

        #region Event methods
        private void Start()
        {
            EventBuilder.GetSingleton().OnAddModeDeactive += GameController_OnAddModeDeactive;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Mouse1) && CurrentMode != EventType.None)
            {
                EventBuilder.GetSingleton().InvokeEvent(EventType.None);
            }
        }

        private void GameController_OnAddModeDeactive()
        {
            CurrentMode = EventType.None;
        }
        #endregion

        #region Buttons
        /// <summary>
        /// B
        /// </summary>
        public void HandleAddBlockButton()
        {
            CurrentMode = EventType.Block;
            EventBuilder.GetSingleton().InvokeEvent(EventType.Block);
        }

        public void HandleAddReachingPointButton()
        {
            CurrentMode = EventType.ReachPoint;
            EventBuilder.GetSingleton().InvokeEvent(EventType.ReachPoint);
        }

        public void HandleAddSpawnerButton()
        {
            CurrentMode = EventType.Spawner;
            EventBuilder.GetSingleton().InvokeEvent(EventType.Spawner);
        }

        public void HandleStartGameButton()
        {
            if(!IsGameStarted)
            {
                if (PathwayCalculator.GetSingleton().ReachingPoint == null)
                {
                    WarningUI.GetSingleton().Enable("Başlamadan önce varış noktası ekle");
                }
                else if (PathwayCalculator.GetSingleton().SpawnerList.Count == 0)
                {
                    WarningUI.GetSingleton().Enable("Başlatmadan önce en az 1 spawner ekle");
                }
                else
                {
                    IsGameStarted = true;
                    PathwayCalculator.GetSingleton().SpawnCars();
                }
            }
        }

        public void HandleRestartButton()
        {
            SceneManager.LoadScene("Game");
        }
        #endregion
    }
}
