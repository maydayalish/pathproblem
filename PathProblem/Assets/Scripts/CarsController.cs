﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameUtility
{
    public class CarsController : MonoBehaviour
    {
        #region singleton creation
        private static CarsController singleton = null;

        public static CarsController GetSingleton()
        {
            if (singleton == null)
            {
                singleton = GameObject.Find("CarsController").GetComponent<CarsController>();
            }
            return singleton;
        }
        #endregion

        [SerializeField] private GameObject carPrefab; //Prefab to instantiate a car
        private List<Car> carList = new List<Car>(); //Holds the data for all car objects in the game



        #region Checkers

        public Car IsCrashPossible(Car car)
        {
            for (int i = 0; i < CarList.Count; i++)
            {
                if (CarList[i] != car && CarList[i].WaitingFor != car && (CarList[i].NextPath == car.NextPath || CarList[i].CurrentPath == car.NextPath))
                {
                    return CarList[i];
                }
            }
            return null;
        }

        public bool IsAnyCarGoingToBlock(Path path)
        {
            for (int i = 0; i < CarList.Count; i++)
            {
                if(path == CarList[i].CurrentPath || path == CarList[i].NextPath)
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

        /// <summary>
        /// When a new calculation needed. It Updates the route data of all currently active cars in the scene
        /// </summary>
        public void UpdateCarsRoute(List<List<Path>> carPaths)
        {
            for (int i = 0; i < CarList.Count; i++)
            {
                CarList[i].PathList = carPaths[i];
            }
        }

        #region Getters Setters
        public List<Car> CarList { get => carList; set => carList = value; }
        #endregion

    }
}
