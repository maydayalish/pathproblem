﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameUtility
{
    public class EventBuilder : MonoBehaviour
    {
        #region Singleton creation
        private static EventBuilder singleton;
        public static EventBuilder GetSingleton()
        {
            if (singleton == null)
            {
                singleton = GameObject.Find("EventBuilder").GetComponent<EventBuilder>();
            }
            return singleton;
        }
        #endregion
        public delegate void SimpleCallback();

        public event SimpleCallback OnReachingPointAddMode; //Event when player wants to add reach point into game
        public event SimpleCallback OnBlockAddMode; //Event when player wants to add block
        public event SimpleCallback OnSpawnPointAddMode; //Event works when player wants to add spawn point
        public event SimpleCallback OnAddModeDeactive; //Event works when any event finished or cancelled

        private Dictionary<EventType, SimpleCallback> eventList = new Dictionary<EventType, SimpleCallback>();

        private void Start()
        {
            eventList.Add(EventType.Block, OnBlockAddMode);
            eventList.Add(EventType.ReachPoint, OnReachingPointAddMode);
            eventList.Add(EventType.Spawner, OnSpawnPointAddMode);
            eventList.Add(EventType.None, OnAddModeDeactive);
        }

        /// <summary>
        /// Works to Invoke any event
        /// </summary>
        public void InvokeEvent(EventType type)
        {
            eventList[type]?.Invoke();
        }
    }
}
