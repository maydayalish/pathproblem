﻿using System.Collections.Generic;
using UnityEngine;

namespace GameUtility
{
    public class Path : MonoBehaviour
    {
        [SerializeField] private List<Path> neighbourPaths; 
        [SerializeField] private bool walkable = true; //Block or not

        private bool blocked = false; //True if Spawner, block or reaching point on this path
        private bool onPathWay = false; //True if this pathway on a car or spawner waypath

        //Temporary values
        [SerializeField] private Path parentPath = null; //Parent connected path to trace the pathway
        private int xPos; //Index positions
        private int yPos; //
        private int currentGCost; //GCost is the distance from starting path
        private int currentHeuristic; //Heuristic is the distance from reaching point

        //Fcost is the total of Heuristic and GCost values
        public int CurrentFCost 
        {
            get
            {
                return CurrentGCost + CurrentHeuristic;
            }
        }

        void Start()
        {
            ResetPath();
        }

        
        public void AddNeighbourPath(Path node)
        {
            this.NeighbourPaths.Add(node);
        }

        /// <summary>
        /// Resets the temporary path values to calculate another objects path
        /// </summary>
        public void ResetPath()
        {
            ParentPath = null;
            CurrentGCost = 0;
            CurrentHeuristic = 0;
        }


        /// <summary>
        /// Gets the position to place the line
        ///  TODO there are problem lines are together. It may be updated in next version
        /// </summary>
        /// <returns>Vector3</returns>
        public Vector3 GetLinePosition()
        {
            return new Vector3(transform.position.x, 0.52f, transform.position.z);
        }

        #region Getters Setters
        public bool Blocked { get => blocked; set => blocked = value; }
        public bool OnPathWay { get => onPathWay; set => onPathWay = value; }
        public List<Path> NeighbourPaths { get => neighbourPaths; set => neighbourPaths = value; }
        public bool Walkable { get => walkable; set => walkable = value; }
        public Path ParentPath { get => parentPath; set => parentPath = value; }
        public int XPos { get => xPos; set => xPos = value; }
        public int YPos { get => yPos; set => yPos = value; }
        public int CurrentGCost { get => currentGCost; set => currentGCost = value; }
        public int CurrentHeuristic { get => currentHeuristic; set => currentHeuristic = value; }
        #endregion
    }
}
