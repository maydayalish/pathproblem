﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace GameUtility
{
    public class Car : MonoBehaviour
    {
        [SerializeField] private LineRenderer currentLine; //Renders a dynamic line to the next path

        private Path currentPath; //The path the car currently on or passed last
        private Path nextPath; //The path the car is going or will go next
        private List<Path> pathList = new List<Path>(); //all pathlist that car will go during its lifetime

        private List<Line> carLines = new List<Line>(); //The line renderer object created specifically for this car

        private int colorID;
        private Car waitingFor = null;

        //Values for splitted path drawing
        private float lineDifference; //Final result for positioning the line
        private float lineWidth; //Line Width
        private bool isHorizontalLine;

        [SerializeField] private MeshRenderer meshRenderer;

        
        /// <summary>
        /// Initialize all data needed for car pathway, line renderers and colors and gives a first pulse to start moving
        /// </summary>
        public void Initialize(int spawnerIndex)
        {
            this.ColorID = spawnerIndex;
            Color carColor = PathwayCalculator.GetSingleton().SpawnerColors[spawnerIndex % PathwayCalculator.GetSingleton().SpawnerColors.Length];
            this.PathList = PathwayCalculator.GetSingleton().GetPathwayList(spawnerIndex);
            CurrentPath = PathList[0];
            NextPath = PathList[1];
            meshRenderer.material.color = carColor;
            currentLine.material.color = carColor;
        }

        public void StartMoving()
        {
            LineDrawer.GetSingleton().DrawNewCarsPath(this);
            PathList.Remove(CurrentPath);
            PathList.Remove(NextPath);
            transform.DOMove(NextPath.transform.position, 2).onComplete = (() => OnReachedPath());
        }

        #region event methods
        
        private void Update()
        {
            Vector3 difference;
            if (isHorizontalLine)
            {
                difference = new Vector3(0, 0, lineDifference);
            }
            else
            {
                difference = new Vector3(lineDifference, 0, 0);
            }
            currentLine.SetPositions(new Vector3[] { GetLinePosition() + difference, NextPath.GetLinePosition() + difference });
        }

        /// <summary>
        /// Works when a car reached its nextpath
        /// </summary>
        public void OnReachedPath()
        {
            CurrentPath = NextPath;
            if (CarLines.Count != 0)
            {
                LineDrawer.GetSingleton().LinesList.Remove(CarLines[0]);
                Destroy(CarLines[0].gameObject);
                if(CarLines[0].LineCountOnPath != 0)
                {
                    lineWidth = 1f / CarLines[0].LineCountOnPath;
                }
                else
                {
                    lineWidth = 1;
                }
                lineDifference = -0.5f + (lineWidth / 2f) + (lineWidth * CarLines[0].LineIndex);
                isHorizontalLine = CarLines[0].IsHorizontal();
                currentLine.startWidth = lineWidth;
                currentLine.endWidth = lineWidth;
                CarLines.RemoveAt(0);
            }
            NextPath = GetNextPath();
            if (NextPath == null)
            {
                return;
            }
            PathList.RemoveAt(0);
            GoNextPath();
        }

        /// <summary>
        /// Gives the pulse to go next path, waits if there are a traffic or possible crash
        /// </summary>
        public void GoNextPath()
        {
            if (NextPath == null)
            {
                return;
            }
            Car crashCar = CarsController.GetSingleton().IsCrashPossible(this);
            if (crashCar == null)
            {
                WaitingFor = null;
                transform.DOMove(NextPath.transform.position, 2).onComplete = (() => OnReachedPath());
            }
            else
            {
                WaitingFor = crashCar;
                Invoke("GoNextPath", 1f);
            }
        }
        #endregion
        /// <summary>
        /// It gets next path, if the car is on reaching point, destroys the car
        /// and removes from cars list in the CarsController
        /// </summary>
        public Path GetNextPath()
        {
            if (PathList.Count == 0)
            {
                CarsController.GetSingleton().CarList.Remove(this);
                Destroy(gameObject);
                return null;
            }
            return PathList[0];
        }
        /// <summary>
        /// LinePosition must be higher point from the car to be visible
        /// It returns the necessary position value
        /// </summary>
        public Vector3 GetLinePosition()
        {

            return new Vector3(transform.position.x, 0.52f, transform.position.z);
        }
        #region Getters Setters

        public Path NextPath { get => nextPath; set => nextPath = value; }
        public Car WaitingFor { get => waitingFor; set => waitingFor = value; }
        public Path CurrentPath { get => currentPath; set => currentPath = value; }
        public int ColorID { get => colorID; set => colorID = value; }
        public List<Line> CarLines { get => carLines; set => carLines = value; }
        public List<Path> PathList { get => pathList; set => pathList = value; }

        #endregion
    }
}
