﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace GameUtility
{
    public class LineDrawer : MonoBehaviour
    {
        #region singleton creation
        private static LineDrawer singleton = null;

        public static LineDrawer GetSingleton()
        {
            if (singleton == null)
            {
                singleton = GameObject.Find("LineDrawer").GetComponent<LineDrawer>();
            }
            return singleton;
        }
        #endregion

        private List<Line> linesList = new List<Line>();

        /// <summary>
        /// Recreates the drawed lines
        /// </summary>
        public void Initialize(List<List<Path>> pathwayLists, List<List<Path>> carsPathwayLists)
        {
            for (int i = 0; i < LinesList.Count; i++)
            {
                Destroy(LinesList[i].gameObject);
            }
            LinesList.Clear();
            for (int i = 0; i < pathwayLists.Count; i++)
            {
                for (int z = 0; z < pathwayLists[i].Count - 1; z++)
                {
                    pathwayLists[i][z].OnPathWay = true;
                    pathwayLists[i][z].OnPathWay = true;
                }
            }
            for (int i = 0; i < carsPathwayLists.Count; i++)
            {
                Car car = CarsController.GetSingleton().CarList[i];
                car.CarLines.Clear();
                int carColorId = car.ColorID;
                for (int z = 0; z < carsPathwayLists[i].Count - 1; z++)
                {
                    Line newLine = Instantiate(new GameObject("Line")).AddComponent<Line>();
                    newLine.LineRenderer = newLine.gameObject.AddComponent<LineRenderer>();
                    newLine.Initialize(carsPathwayLists[i][z], carsPathwayLists[i][z + 1], carColorId);
                    carsPathwayLists[i][z].OnPathWay = true;
                    carsPathwayLists[i][z].OnPathWay = true;
                    LinesList.Add(newLine);
                    car.CarLines.Add(newLine);
                }
            }
            SplitTheIntersectedLines();
        }

        /// <summary>
        /// When a new car is spawned, it draws a line for the car
        /// </summary>
        public void DrawNewCarsPath(Car newCar)
        {
            for (int i = 1; i < newCar.PathList.Count - 1; i++)
            {
                Line newLine = Instantiate(new GameObject("Line")).AddComponent<Line>();
                newLine.LineRenderer = newLine.gameObject.AddComponent<LineRenderer>();
                newLine.Initialize(newCar.PathList[i], newCar.PathList[i+1], newCar.ColorID);
                LinesList.Add(newLine);
                newCar.CarLines.Add(newLine);
            }
            SplitTheIntersectedLines();
        }

        /// <summary>
        /// This method splits the intersected lines to make them all visible
        /// </summary>
        public void SplitTheIntersectedLines()
        {
            List<Line> newLinesList = new List<Line>();
            List<List<Line>> sameLinesLists = new List<List<Line>>();
            
            for (int i = 0; i < LinesList.Count - 1; i++)
            {
                newLinesList.Add(LinesList[i]);
            }
            while (newLinesList.Count > 0)
            {
                List<Line> sameLines = new List<Line>();
                sameLines.Add(newLinesList[0]);
                for (int z = 1; z < newLinesList.Count;  z++)
                {
                    if((newLinesList[0].PathA == newLinesList[z].PathA && newLinesList[0].PathB == newLinesList[z].PathB) || (newLinesList[0].PathB == newLinesList[z].PathA && newLinesList[0].PathA == newLinesList[z].PathB))
                    {
                        sameLines.Add(newLinesList[z]);
                        newLinesList.RemoveAt(z);
                        z--;
                    }
                }
                newLinesList.RemoveAt(0);
                sameLinesLists.Add(sameLines);
            }

            for (int i = 0; i < sameLinesLists.Count; i++)
            {
                int lineCount = sameLinesLists[i].Count;
                for (int z = 0; z < lineCount; z++)
                {
                    sameLinesLists[i][z].SplitLine(lineCount, z);
                }
            }
        }

        public List<Line> LinesList { get => linesList; set => linesList = value; }
    }
}
