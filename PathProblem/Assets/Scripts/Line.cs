﻿using UnityEngine;

namespace GameUtility
{
    public class Line : MonoBehaviour
    {
        private Path pathA; //First path
        private Path pathB; //Second Path
        private LineRenderer lineRenderer; //Line renderer to show path

        private int lineCountOnPath; //Different lines count on the current same path
        private int lineIndex; //Intersection ID;



        /// <summary>
        /// Initializes and renders the line
        /// </summary>
        public void Initialize(Path pA, Path pB, int colorId)
        {
            this.PathA = pA;
            this.PathB = pB;
            colorId = colorId % PathwayCalculator.GetSingleton().SpawnerColors.Length;
            lineRenderer.material.color = PathwayCalculator.GetSingleton().SpawnerColors[colorId];
            lineRenderer.numCapVertices = 4;
            lineRenderer.SetPositions(new Vector3[] { PathA.GetLinePosition(), PathB.GetLinePosition() });
        }

        /// <summary>
        /// Splits the line upon the line count on the same pathway
        /// </summary>
        public void SplitLine(int splitCount, int lineIndex)
        {
            this.LineCountOnPath = splitCount;
            this.LineIndex = lineIndex;
            float width = 1f / splitCount;
            float startDif = -0.5f + (width / 2f) + (width * lineIndex);
            Vector3 difference;
            lineRenderer.startWidth = width;
            lineRenderer.endWidth = width;

            if(IsHorizontal())
            {
                difference = new Vector3(0,0,startDif);
            }
            else
            {
                difference = new Vector3(startDif, 0, 0);
            }
            lineRenderer.SetPositions(new Vector3[] { PathA.GetLinePosition() + difference, PathB.GetLinePosition() + difference});
        }

        /// <summary>
        /// Checks the line if horizontal or vertical
        /// </summary>
        public bool IsHorizontal()
        {
            if(PathA.XPos == PathB.XPos)
            {
                return false;
            }
            return true;
        }

        #region Getters Setters
        public Path PathA { get => pathA; set => pathA = value; }
        public Path PathB { get => pathB; set => pathB = value; }
        public LineRenderer LineRenderer { get => lineRenderer; set => lineRenderer = value; }
        public int LineCountOnPath { get => lineCountOnPath; set => lineCountOnPath = value; }
        public int LineIndex { get => lineIndex; set => lineIndex = value; }
        #endregion

    }
}
