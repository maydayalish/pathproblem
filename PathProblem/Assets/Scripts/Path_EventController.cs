﻿using UnityEngine;

namespace GameUtility
{
    public class Path_EventController : MonoBehaviour
    {

        //When the mouse hovers over the GameObject, it turns to this color (red)
        Color m_BlockModeColor = Color.red;
        Color m_ReachingModeColor = Color.green;

        //This stores the GameObject’s original color
        Color m_OriginalColor;

        //Get the GameObject’s mesh renderer to access the GameObject’s material and color
        MeshRenderer m_Renderer;

        #region event methods
        void Awake()
        {
            m_Renderer = GetComponent<MeshRenderer>();
            m_OriginalColor = m_Renderer.material.color;
            EventBuilder.GetSingleton().OnAddModeDeactive += Path_EventController_OnAddModeDeactive;
            EventBuilder.GetSingleton().OnBlockAddMode += Path_EventController_OnBlockAddMode;
            EventBuilder.GetSingleton().OnReachingPointAddMode += Path_EventController_OnBlockAddMode;
            EventBuilder.GetSingleton().OnSpawnPointAddMode += Path_EventController_OnBlockAddMode;
            this.enabled = false;
        }

        private void Path_EventController_OnBlockAddMode()
        {
            this.enabled = true;
        }

        private void Path_EventController_OnAddModeDeactive()
        {
            m_Renderer.material.color = m_OriginalColor;
            this.enabled = false;
        }
        #endregion

        /// <summary>
        /// Calculates the result of an event depends on the active object mode
        /// </summary>
        private void OnMouseDown()
        {
            Path myPath;
            switch (GameController.GetSingleton().CurrentMode)
            {
                case EventType.Block:
                    myPath = GetComponent<Path>();
                    if(CarsController.GetSingleton().IsAnyCarGoingToBlock(myPath))
                    {
                        EventBuilder.GetSingleton().InvokeEvent(EventType.None);
                        WarningUI.GetSingleton().Enable("Yakınlarda araba objesi olduğu için eklenemedi");
                        return;
                    }
                    if (!myPath.Blocked)
                    {
                        myPath.Walkable = false;
                        if ((myPath.OnPathWay && PathwayCalculator.GetSingleton().CalculatePathWays()) || !myPath.OnPathWay)
                        {
                            myPath.Blocked = true;
                            m_OriginalColor = Color.red;
                        }
                        else
                        {
                            WarningUI.GetSingleton().Enable("Araba ya da spawn noktası erişilemez konumda olduğu için eklenemedi");
                            myPath.Walkable = true;
                        }
                    }
                    else
                    {
                        WarningUI.GetSingleton().Enable("Zaten orada bir obje var");
                    }
                    EventBuilder.GetSingleton().InvokeEvent(EventType.None);
                    break;
                case EventType.ReachPoint:
                    m_Renderer.material.color = m_ReachingModeColor;
                    Path oldReachingPoint = PathwayCalculator.GetSingleton().ReachingPoint;
                    myPath = GetComponent<Path>();
                    if (!myPath.Blocked)
                    {
                        PathwayCalculator.GetSingleton().ReachingPoint = myPath;
                        if (PathwayCalculator.GetSingleton().CalculatePathWays())
                        {
                            if (oldReachingPoint != null)
                            {
                                oldReachingPoint.Blocked = false;
                                oldReachingPoint.GetComponent<Path_EventController>().m_OriginalColor = Color.white;
                            }
                            myPath.Blocked = true;
                            m_OriginalColor = Color.green;
                        }
                        else
                        {
                            WarningUI.GetSingleton().Enable("Varış noktası erişilemez konumda olduğu için taşınamadı");
                            PathwayCalculator.GetSingleton().ReachingPoint = oldReachingPoint;
                        }
                    }
                    else
                    {
                        WarningUI.GetSingleton().Enable("Zaten orada bir obje var");
                    }
                    EventBuilder.GetSingleton().InvokeEvent(EventType.None);
                    break;
                case EventType.Spawner:
                    myPath = GetComponent<Path>();
                    if (!myPath.Blocked)
                    {
                        if (PathwayCalculator.GetSingleton().AddSpawner(myPath))
                        {
                            myPath.Blocked = true;
                            Debug.Log("Spawner success");
                        }
                        else
                        {
                            WarningUI.GetSingleton().Enable("Erişilemez noktaya spawner eklenemez");
                        }
                    }
                    else
                    {
                        WarningUI.GetSingleton().Enable("Zaten orada bir obje var");
                    }
                    EventBuilder.GetSingleton().InvokeEvent(EventType.None);
                    break;
            }
        }

        void OnMouseOver()
        {
            switch (GameController.GetSingleton().CurrentMode)
            {
                case EventType.None: m_Renderer.material.color = m_OriginalColor; break;
                case EventType.Block: m_Renderer.material.color = m_BlockModeColor; break;
                case EventType.ReachPoint: m_Renderer.material.color = m_ReachingModeColor; break;
                case EventType.Spawner: PathwayCalculator.GetSingleton().SetSpawnerObjectPostion(this.transform.position); break;
            }
        }

        void OnMouseExit()
        {
            // Reset the color of the GameObject back to normal
            m_Renderer.material.color = m_OriginalColor;
        }
    }
}
