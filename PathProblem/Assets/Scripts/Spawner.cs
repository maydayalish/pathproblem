﻿using UnityEngine;

namespace GameUtility
{
    public class Spawner : MonoBehaviour
    {
        [SerializeField] private GameObject carPrefab; //Car object to instantiate
        [SerializeField] private float time = 20f; //Time to wait until new car spawned

        private int spawnerIndex; //The Index number for PathCalculator to determine pathway and its color


        /// <summary>
        /// Initializer method to set spawner index and spawn its first car if the game was started
        /// </summary>
        public void Initialize(int sIndex)
        {
            this.spawnerIndex = sIndex;
            if(GameController.GetSingleton().IsGameStarted)
            {
                SpawnACar();
            }
        }

        /// <summary>
        /// If there is no car on the next path, the method spawns a car
        /// otherwise it stays for a second to try again.
        /// </summary>
        public void SpawnACar()
        {
            Car newCar = Instantiate(carPrefab, transform.position, Quaternion.identity).GetComponent<Car>();
            newCar.Initialize(spawnerIndex);
            if(CarsController.GetSingleton().IsCrashPossible(newCar) != null)
            {
                Destroy(newCar.gameObject);
                Invoke("SpawnACar", 1);
                return;
            }
            newCar.StartMoving();
            CarsController.GetSingleton().CarList.Add(newCar);
            Invoke("SpawnACar", time);
        }
    }
}
