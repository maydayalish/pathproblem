﻿using System.Collections.Generic;
using UnityEngine;

namespace GameUtility
{
    public class PathwayCalculator : MonoBehaviour
    {
        #region Singleton creation
        private static PathwayCalculator singleton;
        public static PathwayCalculator GetSingleton()
        {
            if (singleton == null)
            {
                singleton = GameObject.Find("PathwayCalculator").GetComponent<PathwayCalculator>();
            }
            return singleton;
        }
        #endregion

        private List<Path> spawnerList = new List<Path>(); //The path list of the spawners in the game
        private List<Spawner> spawnerObjectList = new List<Spawner>(); //Spawner game objects list
        private List<List<Path>> pathwayList = new List<List<Path>>(); //Default pathway list for each spawners to improve game performance
        private List<List<Path>> carsPathwayList = new List<List<Path>>(); //Specific pathways after calculation for the cars. It's not final. Cars would updates those as they reach a path

        private Path reachingPoint = null; //Final point for all cars
       
        
        //Needed for spawner adding process
        [SerializeField] private Transform spawnerPrefab; //Prefab to instantiate spawners
        [SerializeField] private Color[] spawnerColors; //Color list for spawners, cars and pathway lines
        private Transform spawnerObject; //The spawner object we drag via mouse on paths

        #region event methods
        //Initializes the event registrations
        private void Start()
        {
            EventBuilder.GetSingleton().OnSpawnPointAddMode += PathwayCalculator_OnSpawnPointAddMode;
            EventBuilder.GetSingleton().OnAddModeDeactive += PathwayCalculator_OnAddModeDeactive;
        }

        /// <summary>
        /// When the object adding process is finished or cancelled, this event works
        /// </summary>
        private void PathwayCalculator_OnAddModeDeactive()
        {
            if (spawnerObject != null)
            {
                Destroy(spawnerObject.gameObject);
                spawnerObject = null;
            }
        }

        /// <summary>
        /// Spawn point adding process event
        /// </summary>
        private void PathwayCalculator_OnSpawnPointAddMode()
        {
            if (spawnerObject == null)
                spawnerObject = Instantiate(spawnerPrefab, new Vector3(1000, 1000, 1000), Quaternion.identity);
        }
        #endregion
        /// <summary>
        /// Adds a spawner, Instantiation and adding into data list. 
        /// If the process fails (The spawner could not be added into the path),
        /// It cancels the process
        /// </summary>
        public bool AddSpawner(Path spawner)
        {
            int spawnerColorIndex = SpawnerList.Count % SpawnerColors.Length;
            SpawnerList.Add(spawner);
            if (CalculatePathWays())
            {
                Spawner newSpawnerObject = Instantiate(spawnerPrefab, spawner.transform.position, Quaternion.identity).GetComponent<Spawner>();
                newSpawnerObject.GetComponent<MeshRenderer>().material.color = SpawnerColors[spawnerColorIndex];
                newSpawnerObject.Initialize(SpawnerObjectList.Count);
                SpawnerObjectList.Add(newSpawnerObject);
                return true;
            }
            SpawnerList.RemoveAt(SpawnerList.Count - 1);
            SpawnerObjectList.RemoveAt(SpawnerObjectList.Count - 1);
            return false;
        }


        /// <summary>
        /// This method calculates pathways for both cars and spawners differently. 
        /// 
        /// </summary>
        /// <returns>boolean</returns>
        public bool CalculatePathWays()
        {
            if (ReachingPoint != null)
            {
                if (SpawnerList.Count == 0)
                {
                    return true;
                }
                List<Path> calculatedPath = new List<Path>();
                List<List<Path>> candidatePathwayList = new List<List<Path>>();
                List<List<Path>> candidateCarsPathwayList = new List<List<Path>>();
                for (int i = 0; i < SpawnerList.Count; i++)
                {
                    calculatedPath = CalculatePathWay(SpawnerList[i]);
                    if(calculatedPath == null)
                    {
                        return false;
                    }
                    else
                    {
                        candidatePathwayList.Add(calculatedPath);
                    }
                   
                    if (candidatePathwayList[candidatePathwayList.Count - 1].Count > 1)
                    {
                        if (SpawnerList.Count - 1 == i)
                        {
                            if (CarsController.GetSingleton().CarList.Count == 0)
                            {
                                PathwayList = candidatePathwayList;
                                finishCalculationProcess();
                                return true;
                            }
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                
                for (int i = 0; i < CarsController.GetSingleton().CarList.Count; i++)
                {
                    calculatedPath = CalculatePathWay(CarsController.GetSingleton().CarList[i].NextPath);
                    if(calculatedPath == null)
                    {
                        return false;
                    }
                    else
                    {
                        candidateCarsPathwayList.Add(calculatedPath);
                    }
                    if(CarsController.GetSingleton().CarList.Count - 1 == i)
                    {
                        PathwayList = candidatePathwayList;
                        CarsPathwayList = candidateCarsPathwayList;
                        finishCalculationProcess();
                        return true;
                    }
                }
            }
            else
            {
                return true;
            }
            Debug.Log("Last false");
            return false;
        }

        /// <summary>
        ///
        /// This function calculates tha path for each spawners with A* Algorithm
        /// which is best one for calculating the shortest path
        /// It returns true or false according to the two points accessibility
        /// </summary>
        /// <returns>boolean</returns>
        public List<Path> CalculatePathWay(Path startPath)
        {
            PathGenerator.GetSingleton().ResetAllPaths();
            List<Path> openList = new List<Path>();
            List<Path> closedList = new List<Path>();
            openList.Add(startPath);
            while (openList.Count > 0)
            {
                Path currentPath = openList[0];
                for (int z = 0; z < openList.Count; z++)
                {
                    if (openList[z].CurrentFCost < currentPath.CurrentFCost || (openList[z].CurrentFCost == currentPath.CurrentFCost && openList[z].CurrentHeuristic < currentPath.CurrentHeuristic))
                    {
                        currentPath = openList[z];
                    }
                }
                openList.Remove(currentPath);
                closedList.Add(currentPath);

                if (currentPath == ReachingPoint)
                {
                    return RetraceThePath(startPath);
                }

                foreach (Path neighbour in currentPath.NeighbourPaths)
                {
                    if (!neighbour.Walkable || closedList.Contains(neighbour))
                    {
                        continue;
                    }
                    int newGCost = currentPath.CurrentGCost + 1;
                    if (newGCost < neighbour.CurrentGCost || !openList.Contains(neighbour))
                    {
                        neighbour.CurrentGCost = newGCost;
                        neighbour.CurrentHeuristic = GetDistance(neighbour, ReachingPoint);
                        neighbour.ParentPath = currentPath;

                        if (!openList.Contains(neighbour))
                        {
                            openList.Add(neighbour);
                        }
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Calculation fills the values in path's 
        /// This method retrace to store found waypoint data
        /// </summary>
        /// <returns>boolean</returns>
        public List<Path> RetraceThePath(Path startPath)
        {
            List<Path> pathWay = new List<Path>();
            Path currentPath = ReachingPoint;
            while (currentPath != startPath)
            {
                pathWay.Add(currentPath);
                currentPath = currentPath.ParentPath;
            }
            pathWay.Add(startPath);
            pathWay.Reverse();
            return pathWay;
        }

        /// <summary>
        /// Gets the distance between two paths
        /// </summary>
        /// <returns>boolean</returns>
        public int GetDistance(Path pathA, Path pathB)
        {
            int distanceX = Mathf.Abs(pathA.XPos - pathB.XPos);
            int distanceY = Mathf.Abs(pathA.YPos - pathB.YPos);
            return distanceX + distanceY;
        }

        /// <summary>
        /// This method fills the data
        /// and place visually the path calculated
        /// </summary>
        /// <returns>boolean</returns>
        private void finishCalculationProcess()
        {
            PathGenerator.GetSingleton().SetAllOnPathWayFalse();
            CarsController.GetSingleton().UpdateCarsRoute(CarsPathwayList);
            LineDrawer.GetSingleton().Initialize(PathwayList, CarsPathwayList);
            for (int i = 0; i < CarsPathwayList.Count; i++)
            {
                CarsPathwayList[i].RemoveAt(0);
            }
        }

        /// <summary>
        /// Sets the position for a cosmetic spawner object which follows the mouse cursor
        /// </summary>
        /// <returns>boolean</returns>
        public void SetSpawnerObjectPostion(Vector3 position)
        {
            if (spawnerObject != null)
            {
                spawnerObject.transform.position = position;
            }
        }

        /// <summary>
        /// This method is called when the game started to spawn cars
        /// </summary>
        /// <returns>boolean</returns>
        public void SpawnCars()
        {
            for (int i = 0; i < SpawnerObjectList.Count; i++)
            {
                SpawnerObjectList[i].SpawnACar();
            }
        }

        #region Getters Setters
        public List<Path> GetPathwayList(int pathIndex)
        {
            List<Path> path = new List<Path>();
            for (int i = 0; i < PathwayList[pathIndex].Count; i++)
            {
                path.Add(PathwayList[pathIndex][i]);
            }
            return path;
        }

        public Path ReachingPoint { get => reachingPoint; set => reachingPoint = value; }
        public Color[] SpawnerColors { get => spawnerColors; set => spawnerColors = value; }
        public List<Path> SpawnerList { get => spawnerList; set => spawnerList = value; }
        public List<List<Path>> PathwayList { get => pathwayList; set => pathwayList = value; }
        public List<Spawner> SpawnerObjectList { get => spawnerObjectList; set => spawnerObjectList = value; }
        public List<List<Path>> CarsPathwayList { get => carsPathwayList; set => carsPathwayList = value; }
        #endregion
    }
}
