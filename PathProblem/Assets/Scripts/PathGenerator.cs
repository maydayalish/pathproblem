﻿using System.Collections.Generic;
using UnityEngine;

namespace GameUtility
{
    public class PathGenerator : MonoBehaviour
    {
        #region singleton creation
        /// <summary>
        /// MonoBehaviour cannot be created as a new instance
        /// Used Singleton pattern to reach that single instance from other code blocks
        /// </summary>
        private static PathGenerator singleton = null;

        public static PathGenerator GetSingleton()
        {
            if (singleton == null)
            {
                singleton = GameObject.Find("PathGenerator").GetComponent<PathGenerator>();
            }
            return singleton;
        }
        #endregion
        [SerializeField] private int row = 5;
        [SerializeField] private int column = 5;
        [SerializeField] private float padding = 3f;
        [SerializeField] private Transform pathPrefab;


        private List<Path> pathList = new List<Path>();

        

        void Start()
        {
            this.generateGrid();
            this.generateNeighbours();
        }


        /// <summary>
        /// Generates the grids with Path script
        /// </summary>
        private void generateGrid()
        {
            int counter = 0;
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < column; j++)
                {
                    Path node = Instantiate(pathPrefab, new Vector3((j * padding) + gameObject.transform.position.x, gameObject.transform.position.y, (i * padding) + gameObject.transform.position.z), Quaternion.identity).GetComponent<Path>();
                    node.name = "node (" + counter + ")";
                    node.XPos = j;
                    node.YPos = i;
                    PathList.Add(node);
                    counter++;
                }
            }
        }

        /// <summary>
        /// Generates the neighbours for each pathways.
        /// </summary>
        private void generateNeighbours()
        {
            for (int i = 0; i < PathList.Count; i++)
            {
                Path currentNode = PathList[i];
                int index = i + 1;

                // For those on the left, with no left neighbours
                if (index % column == 1)
                {
                    // We want the node at the top as long as there is a node.
                    if (i + column < column * row)
                    {
                        currentNode.AddNeighbourPath(PathList[i + column]);   // North node
                    }

                    if (i - column >= 0)
                    {
                        currentNode.AddNeighbourPath(PathList[i - column]);   // South node
                    }
                    currentNode.AddNeighbourPath(PathList[i + 1]);     // East node
                }

                // For those on the right, with no right neighbours
                else if (index % column == 0)
                {
                    // We want the node at the top as long as there is a node.
                    if (i + column < column * row)
                    {
                        currentNode.AddNeighbourPath(PathList[i + column]);   // North node
                    }

                    if (i - column >= 0)
                    {
                        currentNode.AddNeighbourPath(PathList[i - column]);   // South node
                    }
                    currentNode.AddNeighbourPath(PathList[i - 1]);     // West node
                }

                else
                {
                    // We want the node at the top as long as there is a node.
                    if (i + column < column * row)
                    {
                        currentNode.AddNeighbourPath(PathList[i + column]);   // North node
                    }

                    if (i - column >= 0)
                    {
                        currentNode.AddNeighbourPath(PathList[i - column]);   // South node
                    }
                    currentNode.AddNeighbourPath(PathList[i + 1]);     // East node
                    currentNode.AddNeighbourPath(PathList[i - 1]);     // West node
                }

            }
        }

        /// <summary>
        ///  There is not only one waypath. Program should resets the g and f values for each time for the paths
        /// </summary>
        public void ResetAllPaths()
        {
            for (int i = 0; i < PathList.Count; i++)
            {
                PathList[i].ResetPath();
            }
        }

        /// <summary>
        ///  If the paths are on the way of cars, it resets the paths for recalculation
        /// </summary>
        public void SetAllOnPathWayFalse()
        {
            for (int i = 0; i < PathList.Count; i++)
            {
                PathList[i].OnPathWay = false;
            }
        }

        #region Getters Setters
        public List<Path> PathList { get => pathList; set => pathList = value; }
        #endregion
    }
}
